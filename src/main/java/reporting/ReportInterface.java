package reporting;

import indicators.SafeZoneUp;
import models.Purchased;
import yahoo.SIDate;
import yahoo.StockDataDriver;
import yahoo.StockDriverFactory;
import yahoo.YahooQueryData;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class ReportInterface {


    //Entrypoint..
//    public static ArrayList<String> createLimitReport(ArrayList<String> stockNames) {
//        LocalDate dateNow = LocalDate.now();
//
//        ArrayList<String> reportLines = new ArrayList<>();
//
//        for(String stockName : stockNames){
//
//            QueryData queryData = getQueryData(dateNow, stockName);
//
//            loopback.LoopBackData loopBackData = getLoopBackData(queryData);
//
//            reportLines.add(stockName + ":" +getLongStop(loopBackData));
//
//        }
//        return reportLines;
//    }

    //Entrypoint..
    public static ArrayList<String> createLimitReport(ArrayList<Purchased> stockNames) {
        LocalDate dateNow = LocalDate.now();

        ArrayList<String> reportLines = new ArrayList<>();

        for(Purchased stockName : stockNames){

            QueryData queryData = getQueryData(dateNow, stockName.getSymbol());

            loopback.LoopBackData loopBackData = getLoopBackData(queryData);

            reportLines.add(stockName + ":" +getLongStop(loopBackData));

        }
        return reportLines;
    }


    private static QueryData getQueryData(LocalDate dateNow, String stockName) {
        QueryData queryData = new QueryData();
        LocalDate dateOneMonthBack = dateNow.minusMonths(1);
        queryData.setSymbol(stockName);
//			queryData.setFromDate(new SIDate("26", "02", "2017"));
//			queryData.setToDate(new SIDate("26", "08", "2017"));

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyyyy");

        String  dateOneMonthBackStr =  dtf.format(dateOneMonthBack);
        String dateNowStr = dtf.format(dateNow);

        System.out.println(dateOneMonthBack.getDayOfMonth());
        System.out.println(dateOneMonthBack.getMonthValue());

        queryData.setFromDate(new SIDate(dateOneMonthBackStr.substring(2,4),
                dateOneMonthBackStr.substring(0,2),
                dateOneMonthBackStr.substring(4,8)));

        queryData.setToDate(new SIDate(dateNowStr.substring(2,4),
                dateNowStr.substring(0,2),
                dateNowStr.substring(4,8)));
        return queryData;
    }


    public static double getLongStop(loopback.LoopBackData loopBackData) {
        //loopback.LoopBackData loopBackData = getLoopBackData(queryData);

        loopback.Stock stock = (loopback.Stock) loopBackData.get(0);

        System.out.println(stock.getDate());

        SafeZoneUp.measureDownSidePen(loopBackData);
        ArrayList<Double> summarizeDownsideList = SafeZoneUp.summarizeDownside(SafeZoneUp.measureDownSidePen(loopBackData), 10);
        SafeZoneUp.isPenetration(loopBackData,10);
        ArrayList<Integer> penetrationSumList = SafeZoneUp.countPen(SafeZoneUp.isPenetration(loopBackData,10),10);

        ArrayList<Double> avgDownsidePenList = SafeZoneUp.avgDownsidePen(summarizeDownsideList,penetrationSumList );

        ArrayList<Double> setShortStopList = SafeZoneUp.setShortStop(loopBackData ,avgDownsidePenList, 2, 10);

        ArrayList<Double> setShortStopRefined = SafeZoneUp.setShortStopRefined(setShortStopList, 10);
        return setShortStopRefined.get(setShortStopRefined.size()-1);
    }

    private static loopback.LoopBackData getLoopBackData(QueryData queryData) {
        YahooQueryData yahooQueryData = new YahooQueryData();
        //yahooQueryData.setSymbol("CASS");
        yahooQueryData.setSymbol(queryData.getSymbol());

        yahooQueryData.setFromDate(queryData.getFromDate());
        yahooQueryData.setToDate(queryData.getToDate());

        StockDataDriver yahooDataDriver = StockDriverFactory.createDriver("YAHOO");

        return YahooQueryData.getLoopBackData(yahooDataDriver.getData(yahooQueryData));
    }





}
