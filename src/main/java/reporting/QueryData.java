package reporting;

import lombok.Getter;
import lombok.Setter;
import yahoo.SIDate;

/**
 * Created by Bjoern on 04-01-2017.
 */
@Getter
@Setter
class QueryData {

    private String symbol;
    private SIDate fromDate;
    private SIDate toDate;

}
