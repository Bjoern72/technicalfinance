package indicators;

import loopback.LoopBackData;
import loopback.Stock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SafeZoneValues {

    public ArrayList<SafeZoneValue> getSafeZone() {
        return safeZone;
    }

    public void setSafeZone(ArrayList<SafeZoneValue> safeZone) {
        this.safeZone = safeZone;
    }

    private ArrayList<SafeZoneValue> safeZone;

    public SafeZoneType getSafeZoneType() {
        return safeZoneType;
    }

    public static SafeZoneType safeZoneType;

    public SafeZoneValues( LoopBackData loopBack, ArrayList<Double> safeZoneValues) {
        this.safeZone = new ArrayList<>();
        this.loopBack = loopBack;
        this.safeZoneValues = safeZoneValues;
        //this.safeZoneType = safeZoneType;

        int length = safeZoneValues.size();

        //LoopBackData<Stock> loopBackExtracted = new LoopBackData<>();
        List<Stock> loopBackExtracted = new LoopBackData<>();

//        for (int i = 1;  i <= safeZoneValues.size(); ++i) {
//
//            loopBackExtracted.add( (Stock) loopBack.get(loopBack.size() - i ));
//
//        }

//        for ( int i = safeZoneValues.size();  i > 0; i-- ) {
//
//            loopBackExtracted.add( (Stock) loopBack.get(loopBack.size() - i -1));
//
//        }

        loopBackExtracted = loopBack.subList( (loopBack.size() ) - safeZoneValues.size(), loopBack.size());

        //Collections.reverse(loopBackExtracted);

        if(!(safeZoneValues.size() == loopBackExtracted.size())){
            throw new IllegalArgumentException();
        }

        for(int i=0; i!=loopBackExtracted.size(); i++){

            Stock stock = (Stock) loopBackExtracted.get(i );
            double safeZonePointValue = safeZoneValues.get(i);

            SafeZoneValue safeZoneValue = new SafeZoneValue( stock, safeZonePointValue);
            safeZone.add(safeZoneValue);


        }



    }

    enum SafeZoneType {

        SAFE_ZONE_LONG,
        SAFE_ZONE_SHORT

    }

    public class SafeZoneValue{

        public Stock getStock() {
            return stock;
        }

        public Double getSafeZoneValue() {
            return safeZoneValue;
        }

        Stock stock;
        Double safeZoneValue;

        public SafeZoneValue(Stock stock, Double safeZoneValue) {
            this.stock = stock;
            this.safeZoneValue = safeZoneValue;
        }
    }

    LoopBackData loopBack;
    ArrayList<Double> safeZoneValues;

}
