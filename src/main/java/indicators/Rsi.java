package indicators;

/*
    Lidgren Technical Stock Analyzer
    Copyright (C) 2002 Lars Lidgren

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

import java.util.LinkedList;

import loopback.Stock;

public class Rsi {

    public double RSI[];
    private LinkedList<Stock> stockList;

    public Rsi(LinkedList stocklist, int RSI_DAYS) {
        // RSI = 100 - (100 / (1 + RS));
	// RS = ((Total gain for RSI_DAYS) / RSI_DAYS)) / ((Total loss for RSI_DAYS) / RSI_DAYS)
	// Written as (RS_POS / RSI_DAYS) / (RS_NEG / RSI_DAYS)
	// After RSI_DAYS the formula uses the smoothened RS formula which is:
	// (((RS_POS * RSI_DAYS) + gain_since_yesterday) / RSI_DAYS) / (((RS_NEG * RSI_DAYS) + loss_since_yesterday) / RSI_DAYS)
	stockList = stocklist;
	int size = stockList.size();
	double rsi_pos = 0;
	double rsi_neg = 0;
	if(size > RSI_DAYS+1) {	
	    RSI = new double[size];
	    // Collect data for the first RSI_DAYS
	    for(int index = 0 ; index < RSI_DAYS ; index++) {
		Stock stock = (Stock)stockList.get(index);
		Stock next = (Stock)stockList.get(index+1);
		if(next.getClose() > stock.getClose())
		    rsi_pos += next.getClose() - stock.getClose();
		else 
		    rsi_neg += stock.getClose()- next.getClose();
		RSI[index] = 0.5;
		index++;
	    }
	    // Calcultate the first RSI_DAYS (RSI should be multiplied with 100, but it's simplier with 
	    // a value between 0 -> 1 when drawing graphics....
	    RSI[RSI_DAYS] = 1 - (1 / (1 + ((rsi_pos / RSI_DAYS) / (rsi_neg / RSI_DAYS))));
	    // Continue calculating the smoothened way.
	    int index = RSI_DAYS + 1;
	    while(index < size) {
		Stock before = (Stock)stockList.get(index - 1);
		Stock stock = (Stock)stockList.get(index);
		double tmp_pos = 0;
		double tmp_neg = 0;
		if(stock.getClose() > before.getClose())
		    tmp_pos = stock.getClose() - before.getClose();
		else
		    tmp_neg = before.getClose() - stock.getClose();
		rsi_pos = ((rsi_pos * (RSI_DAYS - 1)) + tmp_pos) / RSI_DAYS;
		rsi_neg = ((rsi_neg * (RSI_DAYS - 1)) + tmp_neg) / RSI_DAYS;
		RSI[index] = 1 - (1 / (1 + (rsi_pos / rsi_neg)));		
		//System.out.println(RSI[index]);
		index++;
	    }
	    //System.out.println("RSI");
	    //Gui.showRsi++;
	}
    }

    public double getRsi(int index) {
	return RSI[index];
    }
};





