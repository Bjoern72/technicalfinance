package indicators;

import loopback.LoopBackData;
import loopback.Stock;

import java.util.ArrayList;

//import dk.c_contractors.external_data.yahoo.loopback.LoopBackData;
//import dk.c_contractors.external_data.yahoo.loopback.Stock;

public class SafeZoneUp {
	private double dnPen;
	private double sum;
	private double penYesNo;
	private double dnNumb;
	private double dnAvg;
	private double shortStop;
	private double protect;
	
	public double getDnPen() {
		return dnPen;
	}
	public void setDnPen(double dnPen) {
		this.dnPen = dnPen;
	}
	public double getSum() {
		return sum;
	}
	public void setSum(double sum) {
		this.sum = sum;
	}
	public double getPenYesNo() {
		return penYesNo;
	}
	public void setPenYesNo(double penYesNo) {
		this.penYesNo = penYesNo;
	}
	public double getDnNumb() {
		return dnNumb;
	}
	public void setDnNumb(double dnNumb) {
		this.dnNumb = dnNumb;
	}
	public double getDnAvg() {
		return dnAvg;
	}
	public void setDnAvg(double dnAvg) {
		this.dnAvg = dnAvg;
	}
	public double getShortStop() {
		return shortStop;
	}
	public void setShortStop(double shortStop) {
		this.shortStop = shortStop;
	}
	public double getProtect() {
		return protect;
	}
	public void setProtect(double protect) {
		this.protect = protect;
	}

	public static ArrayList<Double> setShortStop(LoopBackData<Stock> loopBackData , ArrayList<Double> avgDownsidePenList, double coefNoiseLevel, int loopback)
	{
		double stop = 0d;
		double todayLow = 0d;
		double yesterdayLow = 0d;

		ArrayList<Double> setShortStopList = new ArrayList<Double>();

		//int j=0;
		for(int i=(loopBackData.size()-avgDownsidePenList.size()); i!=loopBackData.size(); i++)
		{

			if(i>loopback)
				stop = loopBackData.get(i-1).getLow() - (avgDownsidePenList.get(i-1)*coefNoiseLevel);
			//j++;
			//System.out.println(stop);
			setShortStopList.add(stop);
		}
		return setShortStopList;
	}


	public static void setShortStopRefinedOut(ArrayList<Double> setShortStopList)
	{
		double stopRefined=0d;
		for(int i=0;i!=setShortStopList.size();i++)
		{
			if(i>1)
			{
				stopRefined = Math.max(setShortStopList.get(i-2), Math.max(setShortStopList.get(i-1), setShortStopList.get(i)));
			}
			//System.out.println(stopRefined);
		}
	}


	public static ArrayList<Double> setShortStopRefined(ArrayList<Double> setShortStopList, int loopback)
	{
		double stopRefined=0d;

		ArrayList<Double> stopRefinedSeries = new ArrayList<>();

		for(int i=0;i!=setShortStopList.size();i++)
		{
			if(i>loopback)
			{
				stopRefined = Math.max(setShortStopList.get(i-2), Math.max(setShortStopList.get(i-1), setShortStopList.get(i)));
				//stoprefined =
				stopRefinedSeries.add(stopRefined);
			}
			//System.out.println(stopRefined);
		}
		//return stopRefinedSeries.get(stopRefinedSeries.size()-1);
		return stopRefinedSeries;
	}

	public static ArrayList<Double> avgDownsidePen(ArrayList<Double> summarizeDownsideList, ArrayList<Integer> penetrationSumList )
	{
		double avgDownsidePen = 0d;
		ArrayList<Double> avgDownsidePenList = new ArrayList<Double>();

		for(int i=0; i!=summarizeDownsideList.size();i++)
		{
			avgDownsidePen = summarizeDownsideList.get(i)/penetrationSumList.get(i);
			//System.out.println(i + " " + avgDownsidePen);
			avgDownsidePenList.add(avgDownsidePen);
		}
		return avgDownsidePenList;

	}

	public static ArrayList<Double> summarizeDownside(ArrayList<Double> downSidePenList, int loopback)
	{

		double summDonsideAtIdx = 0d;

		ArrayList<Double> summarizeDownsideList = new ArrayList<Double>();



		for(int i=1; i<=downSidePenList.size(); i++)
		{
			if(i>loopback)
			{
				summDonsideAtIdx =0;
				for(int j=(i-loopback); j!=i; j++)
				{
					summDonsideAtIdx += downSidePenList.get(j);
				}

			}
			summarizeDownsideList.add(summDonsideAtIdx);
			//System.out.println(i+ " "+summDonsideAtIdx);

		}
		return summarizeDownsideList;
	}

	public static ArrayList<Integer> countPen(ArrayList<Integer> penetrationList, int loopback)
	{



		int penetration;
		int penCount=0;

		ArrayList<Integer> penetrationSumList = new ArrayList<Integer>() {
		};

		for(int i=1; i<=penetrationList.size();i++){

			if(i>loopback)
			{
				penCount=0;
				for(int j=(i)-loopback; j!=i ;j++)
				{
					penetration = penetrationList.get(j);
					if(penetration>0)
						penCount++;
				}
			}
			//System.out.println(i + " " +penCount );
			penetrationSumList.add(penCount);
		}
		return penetrationSumList;
	}

	public static ArrayList<Integer> isPenetration(LoopBackData<Stock> loopBackData, int loopback)
	{
		double todayLow = 0d;
		double yesterdayLow = 0d;

		boolean isPenetration = false;

		ArrayList<Integer> penetrationList = new ArrayList<Integer>();

		for(int i=0; i!=loopBackData.size();i++){
			todayLow = (loopBackData.get(i)).getLow();
			if(i>0){
				yesterdayLow = (loopBackData.get(i-1)).getLow();

			if(yesterdayLow > todayLow)
			{
				isPenetration = true;

			}
			else
			{
				isPenetration = false;
			}}

			int penetration = (isPenetration == true)?  1 : 0;

			penetrationList.add(penetration);


			//System.out.println(i + " " + isPenetration + " " + penetration );


		}
		return penetrationList;

	}

	/**
	 * Obtain at least  a month of data for your stock (or future) in high-low-close format.
	 *
	 * Test wheater today's low is lower than yesterdays low
	 *
	 * */
	public static ArrayList<Double> measureDownSidePen(LoopBackData<Stock> loopBackData)
	{
		//int i=0;
		
		double todayLow;
		double yesterdayLow = 0d;
		double downSidePen;
		
		ArrayList<Double> downSidePenList = new ArrayList<Double>();
		
		for(int i=0; i!=loopBackData.size();i++){
			
			todayLow = (loopBackData.get(i)).getLow();
			if(i>0)
				yesterdayLow = (loopBackData.get(i-1)).getLow();
			
			if(yesterdayLow > todayLow)
			{
				downSidePen = yesterdayLow - todayLow;  
			}
			else
			{
				downSidePen = 0;
			}
			downSidePenList.add(downSidePen);
			
			//System.out.println(i+" |"+yesterdayLow+"|"+ todayLow+"|"+downSidePen + "|\n");
			
		}
		
		return downSidePenList;
	}
	
	
	public static String print(LoopBackData<Stock> loopBackData)
	{
		
		StringBuffer strBuf = new StringBuffer("[");
		
		strBuf.append("[Date, Open, High, Low, Close, Volume, Adj. Close*]\n");
		for(Stock stock : loopBackData)
		{
			strBuf.append("[");
			strBuf.append(stock.getDate()+", ");
			strBuf.append(stock.getOpen()+ ", ");
			strBuf.append(stock.getHigh()+ ", ");
			strBuf.append(stock.getLow()+ ", ");
			strBuf.append(stock.getClose());
			
			strBuf.append(stock.getVolume());
			//strBuf.append(stock.getAdjClose());
			strBuf.append("]\n");
			
			
		}
		strBuf.append("]");
		return strBuf.toString();
	}	
	
}
