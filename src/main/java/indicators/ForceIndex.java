package indicators;

import loopback.Stock;

import java.util.LinkedList;

import static indicators.MovingAVG.ema;
/**
 * Created by Bjoern on 13-08-2016.
 */
public class ForceIndex {

    //Force index..

    private final static int FORCE_INDEX_2_DAYS = 2;
    private final static int FORCE_INDEX_13_DAYS = 13;

    public static LinkedList<Double> forceIndex(LinkedList<Stock> loopBackData) {

        LinkedList<Double> FORCE_INDEX = new LinkedList<Double>();
        Stock stock;
        double closeToday;
        double volumeToday;
        double closeYesterday;


        for (int i = 0; i != loopBackData.size(); i++) {
            stock = loopBackData.get(i);
            closeToday = stock.getClose();
            if (i > 0)
                closeYesterday = loopBackData.get(i - 1).getClose();
            else
                closeYesterday = 0d;
            volumeToday = stock.getVolume();
            FORCE_INDEX.add((closeToday - closeYesterday) * volumeToday);
            System.out.println(i + " " + FORCE_INDEX.get(i));

        }

        return FORCE_INDEX;

    }


    public static LinkedList<Double> forceEma2(LinkedList<Stock> loopBackData){

        return ema(FORCE_INDEX_2_DAYS, forceIndex(loopBackData));

    }

    public static LinkedList<Double> forceEma13(LinkedList<Stock> loopBackData){

        return ema(FORCE_INDEX_13_DAYS, forceIndex(loopBackData));

    }
}
