package indicators;

import java.util.ArrayList;

public class SafeZoneShort {


    public static ArrayList<Double> getUpsidePen(loopback.LoopBackData<loopback.Stock> loopBackData)
    {
        //int i=0;

        double todayHigh;
        double yesterdayHigh = 0d;
        double upSidePen = 0d;;

        ArrayList<Double> upSidePenList = new ArrayList<Double>();

        for(int i=0; i!=loopBackData.size();i++){

            todayHigh = (loopBackData.get(i)).getHigh();
            if(i>0){
                yesterdayHigh = (loopBackData.get(i-1)).getHigh();

            if( todayHigh >yesterdayHigh)
            {
                upSidePen = todayHigh - yesterdayHigh ;
            }
            else
            {
                upSidePen = 0;
            }}
            upSidePenList.add(upSidePen);

            //System.out.println(i+" |"+yesterdayLow+"|"+ todayLow+"|"+downSidePen + "|\n");

        }

        return upSidePenList;
    }

    public static ArrayList<Integer> isPenetration(loopback.LoopBackData<loopback.Stock> loopBackData, int loopback)
    {
        double todayHigh = 0d;
        double yesterdayHigh = 0d;

        boolean isPenetration = false;

        ArrayList<Integer> penetrationList = new ArrayList<Integer>();

        for(int i=0; i!=loopBackData.size();i++){
            todayHigh = (loopBackData.get(i)).getHigh();
            if(i>0){
                yesterdayHigh = (loopBackData.get(i-1)).getHigh();

            if(todayHigh > yesterdayHigh)
            {
                isPenetration = true;

            }
            else
            {
                isPenetration = false;
            }}

            int penetration = (isPenetration == true)?  1 : 0;

            penetrationList.add(penetration);


            //System.out.println(i + " " + isPenetration + " " + penetration );


        }
        return penetrationList;

    }

    public static ArrayList<Double> summarizeUpside(ArrayList<Double> upSidePenList, int loopback)
    {

        double summUpsideAtIdx = 0d;

        ArrayList<Double> summarizeUpsideList = new ArrayList<Double>();



        for(int i=1; i<=upSidePenList.size(); i++)
        {
            if(i>loopback)
            {
                summUpsideAtIdx =0;
                for(int j=(i-loopback); j!=i; j++)
                {
                    summUpsideAtIdx += upSidePenList.get(j);
                }

            }
            summarizeUpsideList.add(summUpsideAtIdx);
            //System.out.println(i+ " "+summDonsideAtIdx);

        }
        return summarizeUpsideList;
    }

    public static ArrayList<Integer> countPen(ArrayList<Integer> penetrationList, int loopback)
    {

        int penetration;
        int penCount=0;

        ArrayList<Integer> penetrationSumList = new ArrayList<Integer>() {
        };

        for(int i=1; i<=penetrationList.size();i++){

            if(i>loopback)
            {
                penCount=0;
                for(int j=(i)-loopback; j!=i ;j++)
                {
                    penetration = penetrationList.get(j);
                    if(penetration>0)
                        penCount++;
                }
            }
            //System.out.println(i + " " +penCount );
            penetrationSumList.add(penCount);
        }
        return penetrationSumList;
    }


    public static ArrayList<Double> avgDownsidePen(ArrayList<Double> summarizeDownsideList, ArrayList<Integer> penetrationSumList )
    {
        double avgDownsidePen = 0d;
        ArrayList<Double> avgDownsidePenList = new ArrayList<Double>();

        for(int i=1; i!=summarizeDownsideList.size();i++)
        {
            avgDownsidePen = summarizeDownsideList.get(i)/penetrationSumList.get(i);
            //System.out.println(i + " " + avgDownsidePen);
            avgDownsidePenList.add(avgDownsidePen);
        }
        return avgDownsidePenList;

    }

    public static ArrayList<Double> setLongStop(loopback.LoopBackData<loopback.Stock> loopBackData , ArrayList<Double> avgUpsidePenList, double coefNoiseLevel, int loopback)
    {
        double stop = 0d;
        double todayLow = 0d;
        double yesterdayLow = 0d;

        ArrayList<Double> setShortStopList = new ArrayList<Double>();

        //int j=0;
        for(int i=(loopBackData.size()-avgUpsidePenList.size()); i!=loopBackData.size(); i++)
        {

            if(i>loopback)
                stop = loopBackData.get(i-1).getHigh() + (avgUpsidePenList.get(i-1)*coefNoiseLevel);
            //j++;
            //System.out.println(stop);
            setShortStopList.add(stop);
        }
        return setShortStopList;
    }

    public static ArrayList<Double> setLongStopRefined(ArrayList<Double> setLongStopList, int loopback)
    {
        double stopRefined=0d;

        ArrayList<Double> stopRefinedSeries = new ArrayList<>();

        for(int i=0;i!=setLongStopList.size();i++)
        {
            if(i>loopback)
            {
                stopRefined = Math.min(setLongStopList.get(i-2), Math.min(setLongStopList.get(i-1), setLongStopList.get(i)));
                stopRefinedSeries.add(stopRefined);
            }
        }
        return stopRefinedSeries;
    }
}
