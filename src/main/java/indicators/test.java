package indicators;

import java.util.Comparator;
import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;

public class test {

    //https://stackoverflow.com/questions/16255270/final-variable-case-in-switch-statementhttps://stackoverflow.com/questions/16255270/final-variable-case-in-switch-statement

    public static void main(String[] args) {

        final int a = 1;
        final int b = 2;
        final int x = 0;

        switch (x) {
            case a:break;     // ok
            case b:break;     // compiler error: Constant expression required

        }

        test Test = new test();
        Test.testLamdas();


//        final int b;
//        if (something) {
//            b = 1;
//        } else {
//            b = 2;
//        }

    }

    public void testMethod(final int v){

        int x = v;

        final int b = 2;

        final int a = 1;
//        final int b;
//        b = 2;
        //final int x = 0;

        switch (x) {
            case a:break;     // ok
            case b:break;     // compiler error: Constant expression required

        }

    }

    public void testLamdas(){


        Comparator <String> cc =
            (String s1, String s2) -> s1.compareToIgnoreCase(s2);

        IntBinaryOperator[] calculatorOps = new IntBinaryOperator[]{

            (x,y) -> x + y, (x,y) -> x - y, (x,y) -> x * y, (x,y) -> x / y

        };

        System.out.println(calculatorOps.length);
    }


    void foo(){

        //final int i = 2; Runnable r = () -> {int i = 3;};

        //final int i = 2;
        IntUnaryOperator iou = i -> { int j = 3; return i + j; };



    }


    class Foo {

        int i, j=3;

        IntUnaryOperator iou = i -> {int j =3; return i + j;};


    }

}
