package indicators;



import java.math.BigDecimal;
import java.util.LinkedList;

/**
 * Created by Bjoern on 13-08-2016.
 */
public class MovingAVG {

    private static double priorATR;


    static public LinkedList<Double> sma(int MA_LENGTH, LinkedList<Double> loopBackData) {
        LinkedList<Double> MA = new LinkedList<Double>();

        for(int i=0; i!=loopBackData.size(); i++)
        {
            if(i>=MA_LENGTH-1)
            {
                double maPointVal = maPointValue(loopBackData, MA_LENGTH, i);
                MA.add(maPointVal);
            }
            else
            {
                MA.add(0.0d);
            }
            //System.out.println(MA);
        }
        return MA;
    }


    public static LinkedList<Double> atr(int ATR_LENGTH, LinkedList<Double> loopBackData) {

        LinkedList<Double> ATR = new LinkedList<Double>();

        for(int i=0; i!=loopBackData.size(); i++)
        {
            if(i>=ATR_LENGTH-1)
            {
                double atrPointVal = atrPointValue(loopBackData, ATR_LENGTH, i);
                ATR.add(atrPointVal);
            }
            else
            {
                ATR.add(0.0d);
            }
            //System.out.println(MA);
        }
        return ATR;

    }


//    private static double maPointValue(LoopBackData<Stock> loopBackData,
//                                int MA_LENGTH, int i) {
//        double tot=0.0d;
//        for( int k=MA_LENGTH; k!=0; k--)
//        {
//            tot = tot + (loopBackData.get(i-(k-1))).getClose()  ;
//        }
//        double result = 0.0d;
//        try {
//            result = tot/MA_LENGTH;
//        } catch (java.lang.ArithmeticException e) {
//            // TODO Auto-generated catch block
//            System.out.println("Aritme i: "+i + " value: " + new BigDecimal(MA_LENGTH) + " TOT: " + tot + "\n");
//            e.printStackTrace();
//        }
//
//        return result;
//    }

//    public static LinkedList<Double> ema(int EMA_LENGTH, LoopBackData<Stock> loopBackData) {
//
//        double K =  2d/(EMA_LENGTH+1d) ;
//
//        LinkedList<Double> EMA = new LinkedList<Double>();
//        for(int i=0; i!=loopBackData.size(); i++)
//        {
//            EMA.add( emaPointValue(EMA_LENGTH, loopBackData, K, EMA, i));
//            //System.out.println(EMA);
//        }
//        return EMA;
//    }

//    private static double emaPointValue(int EMA_LENGTH,
//                                 LoopBackData<Stock> loopBackData, double K,
//                                 LinkedList<Double> EMA, int i) {
//        if(i>EMA_LENGTH-1)
//        {
//            Stock  stock = loopBackData.get(i);
//            return (stock.getClose()*K)+((EMA.get(i-1))*(1-K));
//        }
//        else if(i==EMA_LENGTH-1)
//        {
//            return maPointValue(loopBackData, EMA_LENGTH, i);
//        }
//        else
//        {
//            return 0.0d;
//        }
//    }

    //Overload using a value in stead of a stock object
    public static LinkedList<Double> ema(int EMA_LENGTH, LinkedList<Double> loopBackData) {

        double K =  2d/(EMA_LENGTH+1d) ;

        LinkedList<Double> EMA = new LinkedList<Double>();
        for(int i=0; i!=loopBackData.size(); i++)
        {
            EMA.add( emaPointValue(EMA_LENGTH, loopBackData, K, EMA, i));
            //System.out.println(EMA);
        }
        return EMA;
    }

    //Overload using a value in steaed of a stock object
    private static double emaPointValue(int EMA_LENGTH,
                                 LinkedList<Double> loopBackData, double K,
                                 LinkedList<Double> EMA, int i) {
        if(i>EMA_LENGTH-1)
        {
            //Stock  stock = loopBackData.get(i);
            return ((loopBackData.get(i)*K)+(EMA.get(i-1)*(1-K)));
        }
        else if(i==EMA_LENGTH-1)
        {
            return maPointValue(loopBackData, EMA_LENGTH, i);
        }
        else
        {
            return 0.0d;
        }

    }

    //Overload using a value in steaed of a stock object
    private static double maPointValue(LinkedList<Double> loopBackData,
                                       int MA_LENGTH, int i) {
        double tot=0.0d;
        for( int k=MA_LENGTH; k!=0; k--)
        {
            tot = tot+loopBackData.get(i-(k-1));
        }
        return  tot/MA_LENGTH;
    }


    //Overload using a value in steaed of a stock object
    /*
    Current ATR = [(Prior ATR x 13) + Current TR] / 14

      - Multiply the previous 14-day ATR by 13.
      - Add the most recent day's TR value.
      - Divide the total by 14
    * */

    private static double atrPointValue(LinkedList<Double> loopBackData,
                                       int ATR_LENGTH, int i) {

        double tot=0.0d;
        double currentATR = 0;

        if(i==ATR_LENGTH-1){

            for( int k=ATR_LENGTH; k!=0; k--)
            {
                double part = loopBackData.get(i-(k-1));
                tot = tot+part;


            }
            currentATR = tot/14;
            priorATR = currentATR;
        }
        double currentTR = loopBackData.get(i);
        if(i>ATR_LENGTH-1){

            currentATR = ( priorATR *(ATR_LENGTH-1)+currentTR)/14;
            priorATR = currentATR;

        }
        //System.out.println(currentATR);

        return  currentATR;
    }
}
