package indicators;

import loopback.LoopBackData;
import loopback.Stock;


import java.util.ArrayList;

public class SafeZoneMath {

    LoopBackData<Stock> loopBackData;

    /**
     *
     * Obtain at least  a month of data for your stock (or future) in high-low-close format.
     *
     * @param loopBackData
     */
    public SafeZoneMath(LoopBackData<Stock> loopBackData) {

        this.loopBackData = loopBackData;

        ArrayList<Double> downSidePen = getDownSidePen();



    }




    /**
     *
     * Test wheater today's low is lower than yesterdays low
     *
     * It measures the depth of the downside penetration below the previous days
     * range, and if there are none it shows zero.
     *
     * @return  An array of double values representing downside penetration
     */
    public ArrayList<Double> getDownSidePen()
    {
        //int i=0;

        double todayLow;
        double yesterdayLow = 0d;
        double downSidePen;

        ArrayList<Double> downSidePenList = new ArrayList<Double>();

        for(int i=0; i!=loopBackData.size();i++){

            todayLow = (loopBackData.get(i)).getLow();
            if(i>0)
                yesterdayLow = (loopBackData.get(i-1)).getLow();

            if(yesterdayLow > todayLow)
            {
                downSidePen = yesterdayLow - todayLow;
            }
            else
            {
                downSidePen = 0;
            }
            downSidePenList.add(downSidePen);

            //System.out.println(i+" |"+yesterdayLow+"|"+ todayLow+"|"+downSidePen + "|\n");

        }

        return downSidePenList;
    }

    public ArrayList<Double> summarizeDownside(ArrayList<Double> downSidePenList, int loopback)
    {

        double summDonsideAtIdx = 0d;

        ArrayList<Double> summarizeDownsideList = new ArrayList<Double>();



        for(int i=0; i<=downSidePenList.size(); i++)
        {
            if(i>loopback)
            {
                summDonsideAtIdx =0;
                for(int j=(i-loopback); j!=i; j++)
                {
                    summDonsideAtIdx += downSidePenList.get(j);
                }

            }
            summarizeDownsideList.add(summDonsideAtIdx);
            //System.out.println(i+ " "+summDonsideAtIdx);

        }
        return summarizeDownsideList;
    }



}
