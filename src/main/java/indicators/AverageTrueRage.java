package indicators;


import loopback.LoopBackData;
import loopback.Stock;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

import static indicators.MovingAVG.atr;
import static java.lang.Math.abs;

;

/**
 * Created by Bjoern on 03-04-2017.
 */
public class AverageTrueRage {

    private final static int COEF = 3;

    LoopBackData<Stock> loopBackData;

    public AverageTrueRage() {

    }

    public AverageTrueRage(LoopBackData<Stock> loopBackData) {
        super();
        this.loopBackData = loopBackData;
        calculateChandlier(loopBackData);
    }


    //extract data according to loopback period


    public static void calculateChandlier(LoopBackData<Stock> loopBackData){

        Stream<Double> highs = loopBackData.stream().map( h -> h.getHigh());

        Optional<Double> highestHigh = highs.reduce(Double::max);

        Stream<Double> lows = loopBackData.stream().map( h -> h.getLow());
        Optional<Double> lowestLow = lows.reduce(Double::min);

        System.out.println(highestHigh.get());
        System.out.println(lowestLow.get());

    }

    private static final int ATR_PERIOD = 14;

    public static String calculateATR(LoopBackData<Stock> loopBackData){

        double TR = 0;

        ArrayList<Double> trueRangeValues = new ArrayList();
        LinkedList<Double> trueRangeLst = new LinkedList<>();

        for(int i =0; i!=loopBackData.size(); i++){

            double m0 =0;
            double m1 =0;
            double m2 =0;
            m0 = abs(loopBackData.get(i).getHigh()-loopBackData.get(i).getLow());
            if(i!=0){

                m1 = abs(loopBackData.get(i).getHigh()-loopBackData.get(i-1).getClose());
                m2 = abs(loopBackData.get(i).getLow()-loopBackData.get(i-1).getClose());
            }
//            else
//                m1m1m1

            BigDecimal bd0 = new BigDecimal(m0);
            bd0 = bd0.setScale(2, RoundingMode.UP);
            //System.out.println(bd0);

            BigDecimal bd1 = new BigDecimal(m1);
            bd1 = bd1.setScale(2, RoundingMode.UP);
            //System.out.println(bd1);

            BigDecimal bd2 = new BigDecimal(m2);
            bd2 = bd2.setScale(2, RoundingMode.UP);
            //System.out.println(bd2);
            //System.out.println(roundDoubleTo2(m2));

            trueRangeValues.add(m0);
            trueRangeValues.add(m1);
            trueRangeValues.add(m2);
            TR = trueRangeValues.stream().reduce(Double::max).get();
            trueRangeLst.add(TR);
            trueRangeValues.clear();
            //System.out.println(roundDoubleTo2(TR));

        }

        List<Double> ATR = atr(14, trueRangeLst);


        //List<Double> highestHighs = getHighestForEachDayInLoopBack(loopBackData);

        List<Double> lowestLows = getLowestLowsForEachDayInLoopBack(loopBackData);

//        for(int i = 0; i!=highestHighs.size(); i++){
//
//            System.out.println(highestHighs.get(i)-(3*ATR.get(i)));
//
//
//        }

        for(int i = 0; i!=lowestLows.size(); i++){

            System.out.println(lowestLows.get(i)+(3*ATR.get(i)));


        }






        return null;
    }

    private static List<Double> getHighestForEachDayInLoopBack(LoopBackData<Stock> loopBackData) {
        List<Double> highestHighs = new ArrayList<>();
        Collections.reverse(loopBackData);
        int origSize = loopBackData.size();
        for (Iterator<Stock> i = loopBackData.iterator(); i.hasNext(); ) {

                //if (loopBackData.size() > origSize - ATR_PERIOD) {
                if ( loopBackData.size() - ATR_PERIOD  >= 0) {
                    Stock item = i.next();
                    Optional<Double> od = loopBackData.subList(0, ATR_PERIOD).stream().map(Stock::getHigh).reduce(Double::max);
                    highestHighs.add(od.get());
                    i.remove();
                } else {
                    highestHighs.add(new Double(0));
                }

                if(highestHighs.size()==origSize) break;
        }
        Collections.reverse(highestHighs);
        return highestHighs;
    }

    private static List<Double> getLowestLowsForEachDayInLoopBack(LoopBackData<Stock> loopBackData) {
        List<Double> lowestLows = new ArrayList<>();
        Collections.reverse(loopBackData);
        int origSize = loopBackData.size();
        for (Iterator<Stock> i = loopBackData.iterator(); i.hasNext(); ) {

            //if (loopBackData.size() > origSize - ATR_PERIOD) {
            if ( loopBackData.size() - ATR_PERIOD  >= 0) {
                Stock item = i.next();
                Optional<Double> od = loopBackData.subList(0, ATR_PERIOD).stream().map(Stock::getLow).reduce(Double::min);
                lowestLows.add(od.get());
                i.remove();
            } else {
                lowestLows.add(new Double(0));
            }

            if(lowestLows.size()==origSize) break;
        }
        Collections.reverse(lowestLows);
        return lowestLows;
    }


    public static String roundDoubleTo2(double in){

        BigDecimal bd2 = new BigDecimal(in);
        bd2 = bd2.setScale(2, RoundingMode.UP);
        //System.out.println(bd2);
        return bd2.toString();

    }

    public static void main(String[] args) {

        LocalDate l1 = LocalDate.of(1, 04, 2010);
        LocalDate l2 = LocalDate.of(  5, 04, 2010);
        LocalDate l3 = LocalDate.of(  6, 04, 2010);
        LocalDate l4 = LocalDate.of(  7, 04, 2010);
        LocalDate l5 = LocalDate.of(  8, 04, 2010);
        LocalDate l6 = LocalDate.of(  9, 04, 2010);
        LocalDate l7 = LocalDate.of( 12 , 04, 2010);
        LocalDate l8 = LocalDate.of(  13, 04, 2010);
        LocalDate l9 = LocalDate.of(  14, 04, 2010);
        LocalDate l10 = LocalDate.of(15, 04, 2010);
        LocalDate l11 = LocalDate.of(16, 04, 2010);
        LocalDate l12 = LocalDate.of(19, 04, 2010);
        LocalDate l13 = LocalDate.of(20, 04, 2010);
        LocalDate l14 = LocalDate.of(21, 04, 2010);
        LocalDate l15 = LocalDate.of(22, 04, 2010);
        LocalDate l16 = LocalDate.of(23, 04, 2010);
        LocalDate l17 = LocalDate.of(26, 04, 2010);
        LocalDate l18 = LocalDate.of(27, 04, 2010);
        LocalDate l19 = LocalDate.of(28, 04, 2010);
        LocalDate l20 = LocalDate.of(29, 04, 2010);
        LocalDate l21= LocalDate.of(30, 04, 2010);
        LocalDate l22 = LocalDate.of(3, 05, 2010);
        LocalDate l23 = LocalDate.of(4, 05, 2010);
        LocalDate l24 = LocalDate.of(5, 05, 2010);
        LocalDate l25 = LocalDate.of(6, 05, 2010);
        LocalDate l26 = LocalDate.of(7, 05, 2010);
        LocalDate l27 = LocalDate.of(10, 05, 2010);
        LocalDate l28 = LocalDate.of(11, 05, 2010);
        LocalDate l29 = LocalDate.of(12, 05, 2010);
        LocalDate l30 = LocalDate.of(13, 05, 2010);

        //Mock stock values to test ATR

        //Values taken from
        Stock stock1  = new Stock(   l1,48.70,47.79,48.16);
        Stock stock2  = new Stock(   l2,48.72,48.14,48.61);
        Stock stock3  = new Stock(   l3,48.90,48.39,48.75);
        Stock stock4  = new Stock(   l4,48.87,48.37,48.63);
        Stock stock5  = new Stock(   l5,48.82,48.24,48.74);
        Stock stock6  = new Stock(   l6,49.05,48.64,49.03);
        Stock stock7  = new Stock(   l7,49.20,48.94,49.07);
        Stock stock8  = new Stock(   l8,49.35,48.86,49.32);
        Stock stock9  = new Stock(   l9,49.92,49.50,49.91);
        Stock stock10 = new Stock( l10,50.19,49.87,50.13);
        Stock stock11 = new Stock( l11,50.12,49.20,49.53);
        Stock stock12 = new Stock( l12,49.66,48.90,49.50);
        Stock stock13 = new Stock( l13,49.88,49.43,49.75);
        Stock stock14 = new Stock( l14,50.19,49.73,50.03);
        Stock stock15 = new Stock( l15,50.36,49.26,50.31);
        Stock stock16 = new Stock( l16,50.57,50.09,50.52);
        Stock stock17 = new Stock( l17,50.65,50.30,50.41);
        Stock stock18 = new Stock( l18,50.43,49.21,49.34);
        Stock stock19 = new Stock( l19,49.63,48.98,49.37);
        Stock stock20 = new Stock( l20,50.33,49.61,50.23);
        Stock stock21 = new Stock( l21,50.29,49.20,49.24);
        Stock stock22 = new Stock( l22,50.17,49.43,49.93);
        Stock stock23 = new Stock( l23,49.32,48.08,48.43);
        Stock stock24 = new Stock( l24,48.50,47.64,48.18);
        Stock stock25 = new Stock( l25,48.32,41.55,46.57);
        Stock stock26 = new Stock( l26,46.80,44.28,45.41);
        Stock stock27 = new Stock( l27,47.80,47.31,47.77);
        Stock stock28 = new Stock( l28,48.39,47.20,47.72);
        Stock stock29 = new Stock( l29,48.66,47.90,48.62);
        Stock stock30 = new Stock( l30,48.79,47.73,47.85);


        LoopBackData<Stock> stocks = new LoopBackData<>();
        //ArrayList<Stock> stocks = new ArrayList<>();


        stocks.add(stock1 );
        stocks.add(stock2 );
        stocks.add(stock3 );
        stocks.add(stock4 );
        stocks.add(stock5 );
        stocks.add(stock6 );
        stocks.add(stock7 );
        stocks.add(stock8 );
        stocks.add(stock9 );
        stocks.add(stock10);
        stocks.add(stock11);
        stocks.add(stock12);
        stocks.add(stock13);
        stocks.add(stock14);
        stocks.add(stock15);
        stocks.add(stock16);
        stocks.add(stock17);
        stocks.add(stock18);
        stocks.add(stock19);
        stocks.add(stock20);
        stocks.add(stock21);
        stocks.add(stock22);
        stocks.add(stock23);
        stocks.add(stock24);
        stocks.add(stock25);
        stocks.add(stock26);
        stocks.add(stock27);
        stocks.add(stock28);
        stocks.add(stock29);
        stocks.add(stock30);

        calculateChandlier(stocks);

        System.out.println(calculateATR(stocks));




        //System.out.println(stocks);
        //stocks.stream().forEach(s -> System.out.println(s));

/*
01-apr-10	48.70	47.79	48.16	 0.91
05-apr-10	48.72	48.14	48.61	 0.58
06-apr-10	48.90	48.39	48.75	 0.51
07-apr-10	48.87	48.37	48.63	 0.50
08-apr-10	48.82	48.24	48.74	 0.58
09-apr-10	49.05	48.64	49.03	 0.41
12-apr-10	49.20	48.94	49.07	 0.26
13-apr-10	49.35	48.86	49.32	 0.49
14-apr-10	49.92	49.50	49.91	 0.42
15-apr-10	50.19	49.87	50.13	 0.32
16-apr-10	50.12	49.20	49.53	 0.92
19-apr-10	49.66	48.90	49.50	 0.76
20-apr-10	49.88	49.43	49.75	 0.45
21-apr-10	50.19	49.73	50.03	 0.46
22-apr-10	50.36	49.26	50.31	 1.10
23-apr-10	50.57	50.09	50.52	 0.48
26-apr-10	50.65	50.30	50.41	 0.35
27-apr-10	50.43	49.21	49.34	 1.22
28-apr-10	49.63	48.98	49.37	 0.65
29-apr-10	50.33	49.61	50.23	 0.72
30-apr-10	50.29	49.20	49.24	 1.09
03-maj-10	50.17	49.43	49.93	 0.74
04-maj-10	49.32	48.08	48.43	 1.24
05-maj-10	48.50	47.64	48.18	 0.86
06-maj-10	48.32	41.55	46.57	 6.77
07-maj-10	46.80	44.28	45.41	 2.52
10-maj-10	47.80	47.31	47.77	 0.49
11-maj-10	48.39	47.20	47.72	 1.19
12-maj-10	48.66	47.90	48.62	 0.76
13-maj-10	48.79	47.73	47.85	 1.06

* */

    }
}
