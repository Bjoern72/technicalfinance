package exitstrategies;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import indicators.SafeZoneShort;
import indicators.SafeZoneUp;
import indicators.SafeZoneValues;
import loopback.LoopBackData;
import loopback.Stock;
import org.junit.Before;
import org.junit.Test;
import yahoo.SIDate;
import yahoo.YahooQueryData;
import yahoo.YahooStockData;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import static org.junit.Assert.*;

public class TestSafeZoneUp {

    loopback.LoopBackData loopBackDataOnlyLow;
    loopback.LoopBackData loopBackDataOnlyHigh;

    String symbol;
    SIDate fromDate;
    SIDate toDate;


    public static LoopBackData getLoopBackData(ArrayList<YahooStockData> yahooData, String attributeType){

        final LoopBackData loopBack = new LoopBackData<Stock>();

        Stock stock;
        for(int i=0; i!= yahooData.size();i++)
        {
            try
            {
                stock= new Stock();


                stock.setDate(yahooData.get(i).getDate());
                stock.setLow(new Double(yahooData.get(i).getLow()));
                stock.setHigh(new Double(yahooData.get(i).getHigh()));

                loopBack.add(stock);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return loopBack;
    }

    public static LoopBackData getLoopBackDataOnlySetLow(ArrayList<YahooStockData> yahooData, String attributeType){

        LoopBackData loopBack = new LoopBackData<Stock>();

        Stock stock;
        for(int i=0; i!= yahooData.size();i++)
        {
            try
            {
                stock = new Stock();

                if(attributeType.equals("LOW")) {
                    stock.setDate(yahooData.get(i).getDate());
                    stock.setLow(new Double(yahooData.get(i).getLow()));
                }
                else if (attributeType.equals("HIGH")) {
                    stock.setDate(yahooData.get(i).getDate());
                    stock.setHigh(new Double(yahooData.get(i).getHigh()));
                }
                loopBack.add(stock);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return loopBack;
    }



    @Before
    public void createMockData(){


        ArrayList<YahooStockData> lows = new ArrayList<>();

        YahooStockData yahooStockDataOnlyLow3 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow4 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow5 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow6 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow7 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow8 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow9 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow10 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow11 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow12 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow13 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow14 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow15 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow16 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow17 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow18 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow19 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow20 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow21 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow22 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow23 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow24 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow25 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow26 = new YahooStockData();
        YahooStockData yahooStockDataOnlyLow27 = new YahooStockData();


        yahooStockDataOnlyLow3.setDate(2018, Month.APRIL.getValue(), 19);
        yahooStockDataOnlyLow4.setDate(2018, Month.APRIL.getValue(), 20);
        yahooStockDataOnlyLow5.setDate(2018, Month.APRIL.getValue(), 23);
        yahooStockDataOnlyLow6.setDate(2018, Month.APRIL.getValue(), 24);
        yahooStockDataOnlyLow7.setDate(2018, Month.APRIL.getValue(), 25);
        yahooStockDataOnlyLow8.setDate(2018, Month.APRIL.getValue(), 26);
        yahooStockDataOnlyLow9.setDate(2018, Month.APRIL.getValue(), 27);
        yahooStockDataOnlyLow10.setDate(2018, Month.APRIL.getValue(), 30);
        yahooStockDataOnlyLow11.setDate(2018, Month.MAY.getValue(), 1);
        yahooStockDataOnlyLow12.setDate(2018, Month.MAY.getValue(), 2);
        yahooStockDataOnlyLow13.setDate(2018, Month.MAY.getValue(), 3);
        yahooStockDataOnlyLow14.setDate(2018, Month.MAY.getValue(), 4);
        yahooStockDataOnlyLow15.setDate(2018, Month.MAY.getValue(), 7);
        yahooStockDataOnlyLow16.setDate(2018, Month.MAY.getValue(), 8);
        yahooStockDataOnlyLow17.setDate(2018, Month.MAY.getValue(), 9);
        yahooStockDataOnlyLow18.setDate(2018, Month.MAY.getValue(), 10);
        yahooStockDataOnlyLow19.setDate(2018, Month.MAY.getValue(), 11);
        yahooStockDataOnlyLow20.setDate(2018, Month.MAY.getValue(), 14);
        yahooStockDataOnlyLow21.setDate(2018, Month.MAY.getValue(), 15);
        yahooStockDataOnlyLow22.setDate(2018, Month.MAY.getValue(), 16);
        yahooStockDataOnlyLow23.setDate(2018, Month.MAY.getValue(), 17);
        yahooStockDataOnlyLow24.setDate(2018, Month.MAY.getValue(), 18);
        yahooStockDataOnlyLow25.setDate(2018, Month.MAY.getValue(), 21);
        yahooStockDataOnlyLow26.setDate(2018, Month.MAY.getValue(), 22);
        yahooStockDataOnlyLow27.setDate(2018, Month.MAY.getValue(), 23);


        yahooStockDataOnlyLow3.setLow("110.30");
        yahooStockDataOnlyLow4.setLow("113.75");
        yahooStockDataOnlyLow5.setLow("111.68");
        yahooStockDataOnlyLow6.setLow("112.28");
        yahooStockDataOnlyLow7.setLow("111.99");
        yahooStockDataOnlyLow8.setLow("113.68");
        yahooStockDataOnlyLow9.setLow("114.55");
        yahooStockDataOnlyLow10.setLow("114.72");
        yahooStockDataOnlyLow11.setLow("114.90");
        yahooStockDataOnlyLow12.setLow("113.74");
        yahooStockDataOnlyLow13.setLow("112.35");
        yahooStockDataOnlyLow14.setLow("111.20");
        yahooStockDataOnlyLow15.setLow("115.00");
        yahooStockDataOnlyLow16.setLow("115.50");
        yahooStockDataOnlyLow17.setLow("115.30");
        yahooStockDataOnlyLow18.setLow("115.20");
        yahooStockDataOnlyLow19.setLow("110.96");
        yahooStockDataOnlyLow20.setLow("111.00");
        yahooStockDataOnlyLow21.setLow("112.50");
        yahooStockDataOnlyLow22.setLow("112.20");
        yahooStockDataOnlyLow23.setLow("113.36");
        yahooStockDataOnlyLow24.setLow("114.90");
        yahooStockDataOnlyLow25.setLow("117.55");
        yahooStockDataOnlyLow26.setLow("117.05");
        yahooStockDataOnlyLow27.setLow("117.10");

        yahooStockDataOnlyLow3.setHigh("115.90");
        yahooStockDataOnlyLow4.setHigh("116.40");
        yahooStockDataOnlyLow5.setHigh("114.05");
        yahooStockDataOnlyLow6.setHigh("114.75");
        yahooStockDataOnlyLow7.setHigh("114.85");
        yahooStockDataOnlyLow8.setHigh("116.70");
        yahooStockDataOnlyLow9.setHigh("116.90");
        yahooStockDataOnlyLow10.setHigh("118.05");
        yahooStockDataOnlyLow11.setHigh("118.65");
        yahooStockDataOnlyLow12.setHigh("118.95");
        yahooStockDataOnlyLow13.setHigh("115.10");
        yahooStockDataOnlyLow14.setHigh("115.86");
        yahooStockDataOnlyLow15.setHigh("117.25");
        yahooStockDataOnlyLow16.setHigh("117.75");
        yahooStockDataOnlyLow17.setHigh("118.18");
        yahooStockDataOnlyLow18.setHigh("118.90");
        yahooStockDataOnlyLow19.setHigh("114.15");
        yahooStockDataOnlyLow20.setHigh("113.18");
        yahooStockDataOnlyLow21.setHigh("114.15");
        yahooStockDataOnlyLow22.setHigh("115.80");
        yahooStockDataOnlyLow23.setHigh("117.09");
        yahooStockDataOnlyLow24.setHigh("117.68");
        yahooStockDataOnlyLow25.setHigh("119.90");
        yahooStockDataOnlyLow26.setHigh("119.70");
        yahooStockDataOnlyLow27.setHigh("118.95");


        lows.add(yahooStockDataOnlyLow3);
        lows.add(yahooStockDataOnlyLow4);
        lows.add(yahooStockDataOnlyLow5);
        lows.add(yahooStockDataOnlyLow6);
        lows.add(yahooStockDataOnlyLow7);
        lows.add(yahooStockDataOnlyLow8);
        lows.add(yahooStockDataOnlyLow9);
        lows.add(yahooStockDataOnlyLow10);
        lows.add(yahooStockDataOnlyLow11);
        lows.add(yahooStockDataOnlyLow12);
        lows.add(yahooStockDataOnlyLow13);
        lows.add(yahooStockDataOnlyLow14);
        lows.add(yahooStockDataOnlyLow15);
        lows.add(yahooStockDataOnlyLow16);
        lows.add(yahooStockDataOnlyLow17);
        lows.add(yahooStockDataOnlyLow18);
        lows.add(yahooStockDataOnlyLow19);
        lows.add(yahooStockDataOnlyLow20);
        lows.add(yahooStockDataOnlyLow21);
        lows.add(yahooStockDataOnlyLow22);
        lows.add(yahooStockDataOnlyLow23);
        lows.add(yahooStockDataOnlyLow24);
        lows.add(yahooStockDataOnlyLow25);
        lows.add(yahooStockDataOnlyLow26);
        lows.add(yahooStockDataOnlyLow27);

        //Collections.reverse(lows);

        loopBackDataOnlyLow = getLoopBackDataOnlySetLow(lows, "LOW");
        loopBackDataOnlyHigh = getLoopBackDataOnlySetLow(lows, "HIGH");

        for(int i = 0; i!=loopBackDataOnlyLow.size(); i++){

            Stock stock = (Stock) loopBackDataOnlyLow.get(i);
            System.out.println(stock.getDate());
        }
//
//
//        Table<String, String, Integer> universityCourseSeatTable
//                = HashBasedTable.create();

        Table<Calendar, Double, Double> loopBackTable = HashBasedTable.create();



    }


    @Test
    public void testSafeZoneUp(){

        ArrayList<Double> dnPen = SafeZoneUp.measureDownSidePen(loopBackDataOnlyLow);
        System.out.println(dnPen);

        ArrayList<Double> summarizeDownsideList = SafeZoneUp.summarizeDownside(dnPen, 10);
        System.out.println(summarizeDownsideList);

        ArrayList<Integer> isPenetratioanLst = SafeZoneUp.isPenetration(loopBackDataOnlyLow,10);
        System.out.println(isPenetratioanLst);

        ArrayList<Integer> penetrationSumList = SafeZoneUp.countPen(SafeZoneUp.isPenetration(loopBackDataOnlyLow,10),10);
        System.out.println(penetrationSumList);

        ArrayList<Double> avgDownsidePenList = SafeZoneUp.avgDownsidePen(summarizeDownsideList,penetrationSumList );
        System.out.println(avgDownsidePenList);

        ArrayList<Double> setShortStopList = SafeZoneUp.setShortStop(loopBackDataOnlyLow ,avgDownsidePenList, 2, 10);
        System.out.println(setShortStopList);

        ArrayList<Double> setShortStopRefined = SafeZoneUp.setShortStopRefined(setShortStopList, 10);
        System.out.println(setShortStopRefined);

        //double d = SafeZoneUp.setShortStopRefined(setShortStopList, 10);
        double d = setShortStopRefined.get(setShortStopRefined.size()-1);
        System.out.println(d);

    }

    @Test
    public void testSafeZoneDown(){

        //final date
        Stock stock = (Stock) loopBackDataOnlyHigh.get(loopBackDataOnlyHigh.size()-1);

        System.out.println(stock.getDate());

        ArrayList<Double> upPen = SafeZoneShort.getUpsidePen(loopBackDataOnlyHigh);
        System.out.println(upPen);

        ArrayList<Double> summarizeUpsideList = SafeZoneShort.summarizeUpside(upPen, 10);
        System.out.println(summarizeUpsideList);

        ArrayList<Integer> isPenetrationList = SafeZoneShort.isPenetration( loopBackDataOnlyHigh, 10);
        System.out.println(isPenetrationList);

        ArrayList<Integer> penetratioanSumLst = SafeZoneShort.countPen(SafeZoneShort.isPenetration(loopBackDataOnlyHigh,10),10);
        System.out.println(penetratioanSumLst);

        ArrayList<Double> avgDownsidePenList = SafeZoneUp.avgDownsidePen(summarizeUpsideList,penetratioanSumLst );
        System.out.println(avgDownsidePenList);

        ArrayList<Double> setLongStopList = SafeZoneShort.setLongStop(loopBackDataOnlyHigh ,avgDownsidePenList, 2, 10);
        System.out.println(setLongStopList);

        ArrayList<Double> setLongStopRefined = SafeZoneShort.setLongStopRefined(setLongStopList, 10);
        System.out.println(setLongStopRefined);

        double d = setLongStopRefined.get(setLongStopRefined.size()-1);
        System.out.println(d);

        SafeZoneValues safeZoneValues = new SafeZoneValues(loopBackDataOnlyHigh, setLongStopRefined);
        for(SafeZoneValues.SafeZoneValue safeZoneValue : safeZoneValues.getSafeZone()){

            System.out.println(" date: " + safeZoneValue.getStock().getDate()+ " safeZoneValue: " + safeZoneValue.getSafeZoneValue() );

        }
    }
}
