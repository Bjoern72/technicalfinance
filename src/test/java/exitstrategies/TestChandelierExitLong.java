package exitstrategies;

import indicators.AverageTrueRage;
import loopback.LoopBackData;
import loopback.Stock;
import org.junit.Before;
import org.junit.Test;
import yahoo.YahooStockData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class TestChandelierExitLong {

    loopback.LoopBackData loopBackData;


    @Before
    public void initCalendar(){

        Calendar c1 = Calendar.getInstance();   c1.set(2013, 03, 26);
        Calendar c2 = Calendar.getInstance();   c2.set(  2013, 03, 27);
        Calendar c3 = Calendar.getInstance();   c3.set(  2013, 03, 28);
        Calendar c4 = Calendar.getInstance();   c4.set(  2013, 04, 1);
        Calendar c5 = Calendar.getInstance();   c5.set(  2013, 04, 2);
        Calendar c6 = Calendar.getInstance();   c6.set(  2013, 04, 3);
        Calendar c7 = Calendar.getInstance();   c7.set( 2013 , 04, 4);
        Calendar c8 = Calendar.getInstance();   c8.set(  2013, 04, 5);
        Calendar c9 = Calendar.getInstance();   c9.set(  2013, 04, 8);
        Calendar c10 = Calendar.getInstance(); c10.set(2013, 04, 9);
        Calendar c11 = Calendar.getInstance(); c11.set(2013, 04, 10);




            ArrayList<YahooStockData> stockData = new ArrayList<>();

            YahooStockData yahooStockDataOnlyLow01 = new YahooStockData();
            YahooStockData yahooStockDataOnlyLow02 = new YahooStockData();
            YahooStockData yahooStockDataOnlyLow03 = new YahooStockData();
            YahooStockData yahooStockDataOnlyLow04 = new YahooStockData();
            YahooStockData yahooStockDataOnlyLow05 = new YahooStockData();
            YahooStockData yahooStockDataOnlyLow06 = new YahooStockData();
            YahooStockData yahooStockDataOnlyLow07 = new YahooStockData();
            YahooStockData yahooStockDataOnlyLow08 = new YahooStockData();
            YahooStockData yahooStockDataOnlyLow09 = new YahooStockData();
            YahooStockData yahooStockDataOnlyLow10 = new YahooStockData();
            YahooStockData yahooStockDataOnlyLow11 = new YahooStockData();

            yahooStockDataOnlyLow01.setLow("66.82");
            yahooStockDataOnlyLow02.setLow("66.82");
            yahooStockDataOnlyLow03.setLow("66.96");
            yahooStockDataOnlyLow04.setLow("66.98");
            yahooStockDataOnlyLow05.setLow("67.15");
            yahooStockDataOnlyLow06.setLow("67.12");
            yahooStockDataOnlyLow07.setLow("67.14");
            yahooStockDataOnlyLow08.setLow("67.07");
            yahooStockDataOnlyLow09.setLow("67.10");
            yahooStockDataOnlyLow10.setLow("67.08");
            yahooStockDataOnlyLow11.setLow("67.87");

            yahooStockDataOnlyLow01.setHigh("68.73");
            yahooStockDataOnlyLow02.setHigh("68.85");
            yahooStockDataOnlyLow03.setHigh("69.06");
            yahooStockDataOnlyLow04.setHigh("69.09");
            yahooStockDataOnlyLow05.setHigh("69.27");
            yahooStockDataOnlyLow06.setHigh("69.18");
            yahooStockDataOnlyLow07.setHigh("68.60");
            yahooStockDataOnlyLow08.setHigh("67.96");
            yahooStockDataOnlyLow09.setHigh("68.24");
            yahooStockDataOnlyLow10.setHigh("68.96");
            yahooStockDataOnlyLow11.setHigh("70.17");

            stockData.add(yahooStockDataOnlyLow01);
            stockData.add(yahooStockDataOnlyLow02);
            stockData.add(yahooStockDataOnlyLow03);
            stockData.add(yahooStockDataOnlyLow04);
            stockData.add(yahooStockDataOnlyLow05);
            stockData.add(yahooStockDataOnlyLow06);
            stockData.add(yahooStockDataOnlyLow07);
            stockData.add(yahooStockDataOnlyLow08);
            stockData.add(yahooStockDataOnlyLow09);
            stockData.add(yahooStockDataOnlyLow10);
            stockData.add(yahooStockDataOnlyLow11);

            Collections.reverse(stockData);

            loopBackData = getLoopBackData(stockData, "ALL");





    }
    public static LoopBackData getLoopBackData(ArrayList<YahooStockData> yahooData, String attributeType){

        final LoopBackData loopBack = new LoopBackData<Stock>();

        Stock stock;
        for(int i=0; i!= yahooData.size();i++)
        {
            try
            {
                stock= new Stock();

                stock.setLow(new Double(yahooData.get(i).getLow()));
                stock.setHigh(new Double(yahooData.get(i).getHigh()));

                loopBack.add(stock);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return loopBack;
    }


    @Test
    public void testChandelier(){


        System.out.println(AverageTrueRage.calculateATR(loopBackData));



    }

}
